(function() {
    'use strict';

    angular
        .module('dashboards')
        .controller('ServicesCtrl', ServicesCtrl);

    ServicesCtrl.$inject = ['$scope', 'OfferedServices','$ionicLoading','$state'];

    /* @ngInject */
    function ServicesCtrl($scope,OfferedServices,$ionicLoading,$state) {
        var vm = this;
        	vm.services = [];

        $ionicLoading.show({
             animation: 'ion-loading-d',
        });

        /*
          Get Services List and Bind It to home page view
         */
        var services = OfferedServices.getServiceList()
        				.query()
        			   	.$promise.then(function(response){
        			   		vm.services = response;
                            $ionicLoading.hide();
        			   	});
        /**
         * Navigate to service quotes page based on type
         * @param  {[string]} serviceType [Type of service]
         */
        vm.goServiceGetQuotes = function(serviceType){
            $state.go(serviceType);
        }
    }
})();