(function() {
    'use strict';

    angular
        .module('dashboards')
        .controller('ResponseListCtrl', ResponseListCtrl);

    ResponseListCtrl.$inject = ['$scope','OfferedServices','$ionicLoading'];

    /* @ngInject */
    function ResponseListCtrl($scope,OfferedServices,$ionicLoading) {
        var vm = this;
        	vm.responses = [];

        $ionicLoading.show({
             animation: 'ion-loading-d',
        });

        /*
           Get all the Request of currently Login User
         */  
        OfferedServices.getResponseList()
                        // FIXME: Have to change query method to get method as It currently for testing query is 
                        // FIXME: being used, in actual API call It should be get instead of query
        			   .query({userId:1})
        			   .$promise.then(function(response){
        			   	 	vm.requests = response;
                            $ionicLoading.hide();
        			   }, function(error){
                            $ionicLoading.hide();
                       });

        /**
         * load More request if have when user scroll down and bind to view in Request List
         * @return {[List]} [description]
         */
        vm.loadMoreResponses = function(){
        			//	TODO: This method need to change for setting Limit of data being recived in API call 
                    OfferedServices.getResponseList()
                        // FIXME: Have to change query method to get method as It currently for testing query is 
                        // FIXME: being used, in actual API call It should be get instead of query
                       .query({userId:1})
                       .$promise.then(function(response){
                            vm.responses = vm.responses.concat(response);
                            $scope.$broadcast('scroll.infiniteScrollComplete');  
                       }, function(error){
                            $ionicLoading.hide();
                       });
                             
        };
    }
})();