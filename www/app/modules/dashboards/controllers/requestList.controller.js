(function() {
    'use strict';

    angular
        .module('dashboards')
        .controller('RequestCtrl', RequestCtrl);

    RequestCtrl.$inject = ['$scope','OfferedServices', '$ionicLoading'];

    /* @ngInject */
    function RequestCtrl($scope,OfferedServices,$ionicLoading) {
        var vm = this;
        	vm.requests = [];

        $ionicLoading.show({
             animation: 'ion-loading-d',
        });

        /*
           Get all the Request of currently Login User
         */  
        OfferedServices.getRequestList()
                        // FIXME: Have to change query method to get method as It currently for testing query is 
                        // FIXME: being used, in actual API call It should be get instead of query
        			   .query({userId:1})
        			   .$promise.then(function(response){
        			   	 	vm.requests = response;
                            $ionicLoading.hide();
        			   }, function(error){
                          $ionicLoading.hide();
                       });

        /**
         * load More request if have when user scroll down and bind to view in Request List
         * @return {[List]} [description]
         */
        vm.loadMoreRequests = function(){
                    OfferedServices.getRequestList()
                        // FIXME: Have to change query method to get method as It currently for testing query is 
                        // FIXME: being used, in actual API call It should be get instead of query
                       .query({userId:1})
                       .$promise.then(function(response){
                            vm.requests = vm.requests.concat(response);
                            $scope.$broadcast('scroll.infiniteScrollComplete');  
                       }, function(error){
                            $ionicLoading.hide();
                       });
                             
        };
    }
})();