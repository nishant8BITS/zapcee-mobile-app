(function() {
	'use strict';

	angular
		.module('dashboards')
		.factory('OfferedServices', OfferedServices);

	OfferedServices.$inject = ['$resource', 'APIEndPoint'];

	/* @ngInject */
	function OfferedServices($resource, APIEndPoint) {
		return {
			getServiceList: getServiceList,
			getRequestList: getRequestList,
			getResponseList: getResponseList
		}

		/**
		 * Return List of services offered by platform
		 * @return {[JSON]}
		 */
		function getServiceList() {
			
			return $resource(APIEndPoint.URL_GET_SERVICES_LIST, {}, {
				get: {
					method: 'GET',
					cache: true
				}
			});
		}

		/**
		 * Return List of Request when User enter in Request tab first time
		 * @return {[JSON]} [List of Request]
		 */
		function getRequestList() {
			
			return $resource(APIEndPoint.URL_GET_REQUESTS_LIST + "/:userId", {
				userId: '@id'
			});
			// return $resource('https://api.myjson.com/bins/42gno', {}, {
			// 	get: {
			// 		method: 'GET',
			// 		cache: true
			// 	}
			// });
		}

		/**
		 * Return List of Response when user in response list first time
		 * @return {[JSON]} [List of Response of current USER]
		 */
		function getResponseList() {
			return $resource(APIEndPoint.URL_GET_RESPONSE_LIST + "/:userId", {
				userId: '@id'
			});
			// return $resource('https://api.myjson.com/bins/4q1f8', {}, {
			// 	get: {
			// 		method: 'GET',
			// 		cache: true
			// 	}
			// });
		}
	}
})();