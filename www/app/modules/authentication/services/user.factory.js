(function() {
    'use strict';

    angular
        .module('authentication')
        .factory('User', User);

    User.$inject = ['$http', 'APIEndPoint'];

    /* @ngInject */
    function User($http,APIEndPoint) {
        return {
        	signUp : signUp,
        	signIn : signIn,
        	changeUserPassword : changeUserPassword
        }

        function signUp(credentials){
        	return $http.post(APIEndPoint.URL_POST_SIGNUP, credentials);
        }

        function signIn(credentials){
        	return $http.post(APIEndPoint.URL_POST_SIGNIN, credentials);
        }

        function changeUserPassword(passwordDetail){
        	return $http.post(APIEndPoint.urlPostChangePassword, passwordDetail);
        }


    }
})();