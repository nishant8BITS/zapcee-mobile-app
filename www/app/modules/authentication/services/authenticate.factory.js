(function() {
    'use strict';

    angular
        .module('authentication')
        .factory('Authentication', Authentication);

    Authentication.$inject = [];

    /* @ngInject */
    function Authentication() {
    	var _this = this;

		_this._data = {
			user: window.user
		};

		return _this._data;
    }	
})();