(function() {
  'use strict';

  angular
    .module('authentication')
    .controller('SignUpCtrl', SignUpCtrl);

  SignUpCtrl.$inject = ['$scope','$state','$ionicPopup', 'CONSTANT', 'User', 'Authentication'];

  /* @ngInject */
  function SignUpCtrl($scope, $state,$ionicPopup, CONSTANT, User, Authentication) {
    var vm = this;
        $scope.authentication = Authentication;

        // If user is signed in then redirect back service page 
        if ($scope.authentication.user) $state.go('services');

    vm.signup = function(signUpForm) {

      var emailAdd = signUpForm.emailAdd.$modelValue,
        password = signUpForm.password.$modelValue,
        firstName = signUpForm.firstName.$modelValue,
        lastName = signUpForm.lastName.$modelValue;


      if (!CONSTANT.EMAIL_REGEX.test(emailAdd)) {
        $ionicPopup.alert({
          title: 'Sign Up Error',
          template: '<div>Your Email Format is invalid</div>',
          okText: 'Okay',
          okType: 'button-stable'
        });
        return false;

      } else if (password.length < CONSTANT.PASSWORD_MIN_LENGTH) {
        $ionicPopup.alert({
          title: 'Sign Up Error',
          template: '<div>Your Password must be at least 5 chaaracters.</div>',
          okText: 'Okay',
          okType: 'button-stable'
        });
        return false;

      } else {
        var credential = {
          username :emailAdd,  // TODO: Have to remove username as It's email address, need to also change in backend API 
          email : emailAdd,
          firstName : firstName,
          lastName: lastName,
          password: password
        }

        User.signUp(credential)
          .success(function(response) {
            // If successfull then we assign the response to global 
            $scope.authentication.user = response;

            // TODO : As for now redirect to Service page, But we have to redirect to page from where user came 
            $state.go('services');
          })
          .error(function(response) {
            $ionicPopup.alert({
              title: 'Sign Up Error',
              template: '<div>'+response.message+'</div>',
              okText: 'Okay',
              okType: 'button-stable'
            });
          });
      }

    };
  }
})();