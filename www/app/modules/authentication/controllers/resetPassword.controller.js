(function() {
    'use strict';

    angular
        .module('authentication')
        .controller('ResetPwdCtrl', ResetPwdCtrl);

    ResetPwdCtrl.$inject = ['$scope','$ionicPopup', 'CONSTANT','$state'];

    /* @ngInject */
    function ResetPwdCtrl($scope, $ionicPopup,$state,CONSTANT) {
        var vm = this;
        $scope.$state = $state;

        vm.goPreviousState = function(){
            $state.go($state.previous.name);
        }

        vm.resetPwd = function(resetPwdForm){

            var emailAdd  = resetPwdForm.emailAdd.$modelValue;

            if(!CONSTANT.EMAIL_REGEX.test(emailAdd)){
              $ionicPopup.alert({
                    title: 'Reset Error', 
                    template: '<div>No acount exists for' + emailAdd + 
                    		  '.Maybe you signed up using a different/incorrect e-mail addrress.</div>', 
                    okText: 'Okay',
                    okType: 'button-stable'
              });
              return false;

            } else{

              // TODO Call service and Passthe data 
              
            }
        }
    }
})();