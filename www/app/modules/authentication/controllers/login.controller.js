(function() {
    'use strict';

    angular
        .module('authentication')
        .controller('LoginCtrl', LoginCtrl);

    LoginCtrl.$inject = ['$scope','$state','$ionicPopup','CONSTANT','User','Authentication'];

    /* @ngInject */
    function LoginCtrl($scope, $state, $ionicPopup, CONSTANT,User,Authentication,$ionicHistory) {
        var vm = this;
            $scope.authentication = Authentication;

        // If user is signed in then redirect back to services page
        if ($scope.authentication.user) $state.go('services');

        vm.doLogin = function(logInForm){

            var emailAdd  = logInForm.emailAdd.$modelValue,
                password  = logInForm.password.$modelValue;


            if(!CONSTANT.EMAIL_REGEX.test(emailAdd) || password.length < CONSTANT.PASSWORD_MIN_LENGTH){
              $ionicPopup.alert({
                    title: 'Log In Error', 
                    template: '<div>Invalid email or password. Please try again.</div>', 
                    okText: 'Okay',
                    okType: 'button-stable'
              });
              return false;

            } else{

              // TODO Call service and pass the data credential
              var credential = {
                username: emailAdd,  // NOTE: username would be removed, as email address is username in this app, When we change backend API then we will change here 
                password : password
              } 
              User.signIn()
                  .success(function(response){
                    $scope.authentication.user = response;

                     // TODO : As for now redirect to Service page, But we have to redirect to page from where user came 
                    $state.go('services');
                  })
                  .error(function(response){
                        if(typeof response === 'undefined' && response === null) return;
                        $ionicPopup.alert({
                            title: 'Log In Error', 
                            template: '<div>' + response.message + '</div>', 
                            okText: 'Okay',
                            okType: 'button-stable'
                      });
                  });
              
            }

        };
    }
})();