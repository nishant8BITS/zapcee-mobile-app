(function() {
	'use strict';

	angular
		.module('authentication')
		.config([
			'$stateProvider',
			'$urlRouterProvider',
			function($stateProvider, $urlRouterProvider) {
				var _viewPath = 'app/modules/authentication/views/';

				$stateProvider
					.state('login',{
						url : '/login',
						templateUrl: _viewPath + "login.view.html",
						controller : 'LoginCtrl as vm'
					})
					.state('signup',{
						url : '/signup',
						templateUrl: _viewPath + "signUp.view.html",
						controller : 'SignUpCtrl as vm'
					})
					.state('resetpassword',{
						url : '/resetpassword',
						templateUrl: _viewPath + "resetPassword.view.html",
						controller : 'ResetPwdCtrl as vm'
					})
					.state('socialSignup',{
						url : '/socialSignup',
						templateUrl: _viewPath + "socialSignup.view.html",
						controller : 'SocialSignupCtrl as vm'
					});

			}
		]);
})();