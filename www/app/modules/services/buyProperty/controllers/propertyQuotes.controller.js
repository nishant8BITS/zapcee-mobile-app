(function() {
    'use strict';

    angular
        .module('hotels')
        .controller('BuyPropertyCtlr', BuyPropertyCtlr);

    BuyPropertyCtlr.$inject = ['$scope','$ionicPopup'];

    /* @ngInject */
    function BuyPropertyCtlr($scope,$ionicPopup) {
        var vm = this;

        $scope.priceSlider = {
		    min: 100,
		    max: 180,
		    ceil: 500,
		    floor: 0
		};

		$scope.translate = function(value){
		    return '₹' + value;
		}
    }
})();