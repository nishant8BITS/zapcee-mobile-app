(function() {
	'use strict';

	angular
		.module('services')
		.config([
			'$stateProvider',
			'$urlRouterProvider',
			function($stateProvider, $urlRouterProvider) {
				var _viewPath = 'app/modules/services/';

				$stateProvider
					.state('hotelsGetQuotes',{
						url : '/hotelsGetQuotes',
						templateUrl: _viewPath + "hotels/views/hotelsGetQuotes.view.html",
						controller : 'HotelsGetQuotesCtrl as vm'
					})
					.state('propertysGetQuotes',{
						url : '/propertysGetQuotes',
						templateUrl: _viewPath + "buyProperty/views/propertyQuotes.view.html",
						controller : 'BuyPropertyCtlr as vm'
					});
			}
		]);
})();