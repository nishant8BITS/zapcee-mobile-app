(function() {
	'use strict';

	angular
		.module('chats')
		.config([
			'$stateProvider',
			'$urlRouterProvider',
			function($stateProvider, $urlRouterProvider) {
				var _viewPath = 'app/modules/chats/views/';

				$stateProvider
					.state('chatDetails',{
						url : '/chatDetails',
						templateUrl: _viewPath + "chatProviderDetail.view.html",
						controller : 'ChatDetailCtrl as vm'
					});

			}
		]);
})();