(function() {
    'use strict';

    angular
        .module('core')

        /*
         * API EndPoint Constant File Used through out Application
         */
        .constant('APIEndPoint', (function(){
            var baseUrl = 'http://localhost:3002/', //'http://10.0.2.2:3002/',
        	    urlGetServicesList = baseUrl + 'services',
                urlGetRequestList = baseUrl + 'request',
                urlGetResponseList = baseUrl + 'response',

                // User Management Authentication Constant
                urlPostSignUp = baseUrl + 'auth/signup',
                urlPostSignIn = baseUrl + 'auth/signin',
                urlPostForgotPassword = baseUrl + 'auth/forgot',
                urlPostChangePassword = baseUrl + 'users/password';


        	return {
        		URL_GET_SERVICES_LIST : urlGetServicesList,
                URL_GET_REQUESTS_LIST : urlGetRequestList,
                URL_GET_RESPONSE_LIST : urlGetResponseList,

                // USER Authentication URL 
                URL_POST_SIGNUP : urlPostSignUp,
                URL_POST_SIGNIN : urlPostSignIn,
                URL_POST_FGT_PASSWORD : urlPostForgotPassword,
                URL_POST_CHANGE_PASSWORD : urlPostChangePassword
        	}
        })());
})();

