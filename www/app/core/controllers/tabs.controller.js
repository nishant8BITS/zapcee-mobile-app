(function() {
    'use strict';

    angular
        .module('core')
        .controller('TabsCtrl', TabsCtrl);

    TabsCtrl.$inject = ['$scope', '$state','$ionicHistory','$location'];

    /* @ngInject */
    function TabsCtrl($scope, $state,$ionicHistory,$location) {
        var vm = this;
        vm.$state = $state; //Set active class for highlight tabs
    }
})();