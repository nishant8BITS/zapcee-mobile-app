(function() {
    'use strict';

    angular
        .module('core')
        .controller('MenuCtrl', MenuCtrl);

    MenuCtrl.$inject = ['$scope','$state'];

    /* @ngInject */
    function MenuCtrl($scope,$state) {
        var vm = this;

        vm.doLoginLogout = function(){

        	// Check if user is login then do logout
        	// otherwise do login 
        	$state.go('socialSignup');
        }

        vm.goToInviteFrnd = function(){
            $state.go('inviteFriend');
        }
    }
})();