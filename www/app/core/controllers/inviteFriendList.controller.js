(function() {
    'use strict';

    angular
        .module('core')
        .controller('FriendListCtrl', FriendListCtrl);

    FriendListCtrl.$inject = ['$scope','$cordovaContacts','$ionicPlatform'];

    /* @ngInject */
    function FriendListCtrl($scope,$cordovaContacts,$ionicPlatform) {
        var vm = this;
 
        $ionicPlatform.ready(function() {
            $cordovaContacts.find({filter: ''}).then(function(allContacts) { 
            //omitting parameter to .find() causes all contacts to be returned
                $scope.phoneContacts = allContacts;
            });
        });
    }
})();