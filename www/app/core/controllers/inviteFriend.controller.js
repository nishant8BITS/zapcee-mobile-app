(function() {
    'use strict';

    angular
        .module('core')
        .controller('InviteFriendCtrl', InviteFriendCtrl);

    InviteFriendCtrl.$inject = ['$scope','$state'];

    /* @ngInject */
    function InviteFriendCtrl($scope,$state) {
        var vm = this;

        vm.inviteFriend = function(){

        	// Check if user is login then do logout
        	// otherwise do login 
        	$state.go('inviteFriendList');
        }
    }
})();