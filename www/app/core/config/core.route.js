(function() {
	'use strict';

	angular
		.module('core')
		.config([
			'$stateProvider',
			'$urlRouterProvider',
			function($stateProvider, $urlRouterProvider) {
				$urlRouterProvider.otherwise("app/tab/services");

				$stateProvider
					.state('app',{
						url : '/app',
						abstract : true,
						templateUrl:'app/core/views/menu.view.html',
						controller : 'MenuCtrl as vm'
					})
					.state('app.tab',{
						url : '/tab',
						views : {
							'menuContent' : {
								templateUrl:'app/core/views/tabs.view.html',
								controller : 'TabsCtrl as vm'
							}
						}
					})
					.state('app.tab.services',{
						url : '/services',
						views: {
							'mainContent' : {
								templateUrl : 'app/modules/dashboards/views/services.view.html',
								controller : 'ServicesCtrl as vm'
							}
						}
					})
					.state('app.tab.services.detail',{
						url: '/detail/:servicesId',
						views: {
							'menuContent' : {
								templateUrl : '',
								controller : ''
							}
						}
					})
					.state('app.tab.requests',{
						url: '/requests/:userId',
						views: {
							'mainContent' : {
								templateUrl : 'app/modules/dashboards/views/requestList.view.html',
								controller : 'RequestCtrl as vm'
							}
						}
					})
					.state('app.tab.responses',{
						url: '/responses/:userId',
						views: {
							'mainContent' : {
								templateUrl : 'app/modules/dashboards/views/responseList.view.html',
								controller : 'ResponseListCtrl as vm'
							}
						}
					})
					.state('app.tab.chats',{
						url: '/chats',
						views: {
							'mainContent' : {
								templateUrl : 'app/modules/chats/views/chatProviderList.view.html',
								controller : 'ChatListCtrl as vm'
							}
						}
					})
					.state('inviteFriend',{
						url: '/inviteFriend',
						templateUrl : 'app/core/views/inviteFriend.view.html',
						controller : 'InviteFriendCtrl as vm'
					})
					.state('inviteFriendList',{
						url: '/inviteFriendList',
						templateUrl : 'app/core/views/inviteFriendList.view.html',
						controller : 'FriendListCtrl as vm'
					});
			}
		]);
})();