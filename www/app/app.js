/*
* angular.module is a global place for creating, registering and retrieving Angular modules
* 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
* the 2nd parameter is an array of 'requires'
*/

/*
  TODO: When Change app name then Please change the app module name and also 
        index file ng-app and in config file
 */

(function() {
    'use strict';

    angular
        .module('quickly', [
            'ionic',
            'ngCordova',
            'core',
            'ngResource',
            'app.modules',
            'angularMoment',
            'monospaced.elastic',
            'ionic-datepicker',
            'locationSearch',
            'rzModule'
        ])
        .run(initialAppRunSetup)

        /**
         * [initialAppRunSetup]: This function will run at the start of application
         * @param  {[DI]} $ionicPlatform [To come up with Ionic Platform check method]
         */
        function initialAppRunSetup($ionicPlatform,$ionicHistory,$rootScope){
            $ionicPlatform.ready(function() {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if(window.cordova && window.cordova.plugins.Keyboard) {
              cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if(window.StatusBar) {
              StatusBar.styleDefault();
            }
          });

            $rootScope.goPreviousState = function(){
              $ionicHistory.goBack();
            }
        }

})();
